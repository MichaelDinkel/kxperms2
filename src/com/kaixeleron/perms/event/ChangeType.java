package com.kaixeleron.perms.event;

public enum ChangeType {

    /**
     * Used to represent a permission being set to true
     */
    TRUE,

    /**
     * Used to represent a permission being set to false
     */
    FALSE,

    /**
     * Used to represent a permission being unset
     */
    REMOVE

}

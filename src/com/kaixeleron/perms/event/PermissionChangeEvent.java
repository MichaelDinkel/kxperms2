package com.kaixeleron.perms.event;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import java.util.Map;

/**
 * This event is fired when kxPerms changes a permission on an online player either via command or API.
 */
public class PermissionChangeEvent extends Event implements Cancellable {

    private static final HandlerList handlers = new HandlerList();

    private final Player p;

    private final Map<String, ChangeType> permissions;

    private boolean cancelled;

    public PermissionChangeEvent(Player p, Map<String, ChangeType> permissions) {

        this.p = p;

        this.permissions = permissions;

    }

    /**
     * Find if the event has been cancelled.
     * @return True if the event has been cancelled, false otherwise.
     */
    @Override
    public boolean isCancelled() {

        return cancelled;

    }

    /**
     * Set if the event has been cancelled.
     * @param cancelled Whether or not to cancel the event
     */
    @Override
    public void setCancelled(boolean cancelled) {

        this.cancelled = cancelled;

    }

    @Override
    public HandlerList getHandlers() {

        return handlers;

    }

    public static HandlerList getHandlerList() {

        return handlers;

    }

    /**
     * Get the player on which the permission change is happening.
     * @return The player on which the permission change is happening.
     */
    public Player getPlayer() {

        return p;

    }

    /**
     * Get a list of permissions being changed.
     * @see com.kaixeleron.perms.event.ChangeType for possible values
     * @return A map containing all permission changes.
     */
    public Map<String, ChangeType> getPermissions() {

        return permissions;

    }
}

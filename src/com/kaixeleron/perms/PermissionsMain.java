package com.kaixeleron.perms;

import com.kaixeleron.perms.command.PermissionsCommand;
import com.kaixeleron.perms.command.PermissionsTabComplete;
import com.kaixeleron.perms.command.SuCommand;
import com.kaixeleron.perms.database.PermissionsDatabase;
import com.kaixeleron.perms.database.SQLDatabase;
import com.kaixeleron.perms.database.YAMLDatabase;
import com.kaixeleron.perms.listener.ChatListener;
import com.kaixeleron.perms.listener.ConnectListener;
import com.kaixeleron.perms.permission.PermissionManager;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionAttachment;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.IOException;
import java.sql.SQLException;

public class PermissionsMain extends JavaPlugin {

    private PermissionsDatabase database = null;

    private PermissionManager manager;

    private PermissionsCommand command;
    private PermissionsTabComplete tabComplete;

    private PermissionAttachment consoleAttachment;

    @Override
    public void onEnable() {

        saveDefaultConfig();

        switch (getConfig().getString("database")) {

            case "sql":

                ConfigurationSection sql = getConfig().getConfigurationSection("sql");

                try {

                    database = new SQLDatabase(sql.getString("host"), sql.getInt("port"), sql.getString("database"), sql.getString("username"), sql.getString("password"));

                } catch (SQLException e) {

                    System.err.println("Could not connect to MySQL.");
                    e.printStackTrace();
                    setEnabled(false);
                    return;

                }

                break;

            case "api":

                //load nothing so another plugin can add something

                new BukkitRunnable() {

                    @Override
                    public void run() {

                        if (database == null) getLogger().severe("No database has been loaded. kxPerms will not function properly.");

                    }

                }.runTaskLater(this, 20L);

                break;

            case "yaml":
            default:

                try {

                    database = new YAMLDatabase(this);

                } catch (IOException e) {

                    System.err.println("Could not load the YAML database.");
                    e.printStackTrace();
                    setEnabled(false);
                    return;

                }

                break;

        }

        manager = new PermissionManager(this, database);

        command = new PermissionsCommand(database, manager);
        tabComplete = new PermissionsTabComplete(database);

        getCommand("kxp").setExecutor(command);
        getCommand("kxp").setTabCompleter(tabComplete);
        getCommand("su").setExecutor(new SuCommand(this, manager));

        getServer().getPluginManager().registerEvents(new ConnectListener(manager), this);
        if (getConfig().getBoolean("enableChatIntegration")) getServer().getPluginManager().registerEvents(new ChatListener(manager, getConfig().getString("chatFormat")), this);

        consoleAttachment = getServer().getConsoleSender().addAttachment(this);
        consoleAttachment.setPermission("kxperms.*", true);

        for (Player p : getServer().getOnlinePlayers()) {

            manager.handleJoin(p);

        }

        new PermissionsAPI(this, database, manager);

    }

    @Override
    public void onDisable() {

        consoleAttachment.remove();
        database.close();

        for (Player p : getServer().getOnlinePlayers()) {

            manager.handleLeave(p);

        }

    }

    void changeDatabase(PermissionsDatabase database) {

        this.database = database;
        manager.changeDatabase(database);
        command.changeDatabase(database);
        tabComplete.changeDatabase(database);

    }

}

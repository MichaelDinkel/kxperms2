package com.kaixeleron.perms.permission;

import com.kaixeleron.perms.PermissionsMain;
import com.kaixeleron.perms.database.DatabaseException;
import com.kaixeleron.perms.database.PermissionsDatabase;
import com.kaixeleron.perms.event.ChangeType;
import com.kaixeleron.perms.event.PermissionChangeEvent;
import static com.kaixeleron.perms.event.ChangeType.*;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.permissions.PermissionAttachment;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.*;

public class PermissionManager {

    private final PermissionsMain m;

    private final Map<Player, PermissionAttachment> attachments;
    private final Map<Player, String> prefixes, suffixes;
    private final Map<Player, OfflinePlayer> substitutions;

    private PermissionsDatabase database;

    public PermissionManager(PermissionsMain m, PermissionsDatabase database) {

        this.m = m;

        attachments = new HashMap<>();
        prefixes = Collections.synchronizedMap(new HashMap<>());
        suffixes = Collections.synchronizedMap(new HashMap<>());
        substitutions = new HashMap<>();

        this.database = database;

    }

    public void setPlayerPermission(Player target, String permission, boolean value) {

        PermissionChangeEvent event = new PermissionChangeEvent(target, Collections.singletonMap(permission, value ? TRUE : FALSE));
        Bukkit.getPluginManager().callEvent(event);

        if (!event.isCancelled()) {

            PermissionAttachment a = attachments.get(target);

            if (a != null) {

                a.setPermission(permission, value);

            }

            target.updateCommands();

        }

    }

    public void removePlayerPermission(Player target, String permission) {

        PermissionChangeEvent event = new PermissionChangeEvent(target, Collections.singletonMap(permission, REMOVE));
        Bukkit.getPluginManager().callEvent(event);

        if (!event.isCancelled()) {

            PermissionAttachment a = attachments.get(target);

            if (a != null) {

                a.unsetPermission(permission);

            }

            target.updateCommands();

        }

    }

    public void addPlayerGroup(Player target, String group) throws DatabaseException {

        Map<String, Boolean> perms = new HashMap<>();
        List<String> inheritance = getGroupInheritance(group);
        
        for (String s : inheritance) {
            
            perms.putAll(database.getPermissions(s));
            
        }
        
        perms.putAll(database.getPermissions(group));

        Map<String, ChangeType> eventPerms = new HashMap<>();

        for (String s : perms.keySet()) {

            eventPerms.put(s, perms.get(s) ? TRUE : FALSE);

        }

        PermissionChangeEvent event = new PermissionChangeEvent(target, eventPerms);
        Bukkit.getPluginManager().callEvent(event);

        if (!event.isCancelled()) {

            PermissionAttachment a = attachments.get(target);

            for (String s : perms.keySet()) {

                a.setPermission(s, perms.get(s));

            }

            target.updateCommands();

        }

        String prefix = database.getPrefix(group), suffix = database.getSuffix(group);

        for (Player p : Bukkit.getOnlinePlayers()) {

            if (database.getPrefix(p.getUniqueId().toString()) == null) {

                if (database.getInheritance(p.getUniqueId().toString()).contains(group)) {

                    setPlayerPrefix(p, prefix);

                }

            }

            if (database.getSuffix(p.getUniqueId().toString()) == null) {

                if (database.getInheritance(p.getUniqueId().toString()).contains(group)) {

                    setPlayerSuffix(p, suffix);

                }

            }

        }

    }

    public void removePlayerGroup(Player target, String group) throws DatabaseException {

        Map<String, Boolean> perms = new HashMap<>();
        List<String> inheritance = getGroupInheritance(group);

        for (String s : inheritance) {

            perms.putAll(database.getPermissions(s));

        }

        perms.putAll(database.getPermissions(group));

        Map<String, ChangeType> eventPerms = new HashMap<>();

        for (String s : perms.keySet()) {

            eventPerms.put(s, REMOVE);

        }

        PermissionChangeEvent event = new PermissionChangeEvent(target, eventPerms);
        Bukkit.getPluginManager().callEvent(event);

        if (!event.isCancelled()) {

            PermissionAttachment a = attachments.get(target);

            for (String s : perms.keySet()) {

                a.unsetPermission(s);

            }

            target.updateCommands();

        }

        for (Player p : Bukkit.getOnlinePlayers()) {

            List<String> playerInheritance = database.getInheritance(p.getUniqueId().toString());

            if (database.getPrefix(p.getUniqueId().toString()) == null) {

                if (playerInheritance.contains(group)) {

                    for (String parent : playerInheritance) {

                        if (!parent.equals(group)) {

                            setPlayerPrefix(p, database.getPrefix(parent));

                            break;

                        }

                    }

                }

            }

            if (database.getSuffix(p.getUniqueId().toString()) == null) {

                if (playerInheritance.contains(group)) {

                    for (String parent : playerInheritance) {

                        if (!parent.equals(group)) {

                            setPlayerSuffix(p, database.getSuffix(parent));

                            break;

                        }

                    }

                }

            }

        }

    }

    private List<String> getGroupInheritance(String group) throws DatabaseException {

        List<String> out = database.getInheritance(group);

        List<String> parentInheritance = new ArrayList<>();

        for (String s : out) {

            parentInheritance.addAll(getGroupInheritance(s));

        }

        out.addAll(parentInheritance);

        return out;

    }

    public void setPlayerPrefix(Player target, String prefix) {

        synchronized (prefixes) {

            prefixes.put(target, prefix);

        }

    }

    public void setPlayerSuffix(Player target, String suffix) {

        synchronized (suffixes) {

            suffixes.put(target, suffix);

        }

    }

    public void removePlayerPrefix(Player target) {

        synchronized (prefixes) {

            prefixes.remove(target);

        }

    }

    public void removePlayerSuffix(Player target) {

        synchronized (suffixes) {

            suffixes.remove(target);

        }

    }

    public String getPlayerPrefix(Player target) {

        synchronized (prefixes) {

            return prefixes.get(target);

        }

    }

    public String getPlayerSuffix(Player target) {

        synchronized (suffixes) {

            return suffixes.get(target);

        }

    }

    public void setGroupPermission(String target, String permission, boolean value) throws DatabaseException {
        
        for (Player p : Bukkit.getOnlinePlayers()) {
            
            List<String> groups = database.getInheritance(p.getUniqueId().toString());
            
            if (groups.contains(target)) {

                PermissionChangeEvent event = new PermissionChangeEvent(p, Collections.singletonMap(permission, value ? TRUE : FALSE));
                Bukkit.getPluginManager().callEvent(event);

                if (!event.isCancelled()) {

                    attachments.get(p).setPermission(permission, value);

                    p.updateCommands();

                }
                
            } else {
                
                for (String s : groups) {
                    
                    List<String> inheritance = getGroupInheritance(s);
                    
                    if (inheritance.contains(target)) {

                        PermissionChangeEvent event = new PermissionChangeEvent(p, Collections.singletonMap(permission, value ? TRUE : FALSE));
                        Bukkit.getPluginManager().callEvent(event);

                        if (!event.isCancelled()) {

                            attachments.get(p).setPermission(permission, value);

                            p.updateCommands();

                        }
                        
                    }
                    
                }
                
            }
            
        }

    }

    public void removeGroupPermission(String target, String permission) throws DatabaseException {

        for (Player p : Bukkit.getOnlinePlayers()) {

            List<String> groups = database.getInheritance(p.getUniqueId().toString());

            if (groups.contains(target)) {

                PermissionChangeEvent event = new PermissionChangeEvent(p, Collections.singletonMap(permission, REMOVE));
                Bukkit.getPluginManager().callEvent(event);

                if (!event.isCancelled()) {

                    attachments.get(p).unsetPermission(permission);

                    p.updateCommands();

                }

            } else {

                for (String s : groups) {

                    List<String> inheritance = getGroupInheritance(s);

                    if (inheritance.contains(target)) {

                        PermissionChangeEvent event = new PermissionChangeEvent(p, Collections.singletonMap(permission, REMOVE));
                        Bukkit.getPluginManager().callEvent(event);

                        if (!event.isCancelled()) {

                            attachments.get(p).unsetPermission(permission);

                            p.updateCommands();

                        }

                    }

                }

            }

        }
        
    }

    public void addGroupParent(String target, String parent) throws DatabaseException {

        List<String> parents = getGroupInheritance(parent);

        Map<String, Boolean> perms = new HashMap<>();

        for (String s : parents) {

            perms.putAll(database.getPermissions(s));

        }

        perms.putAll(database.getPermissions(parent));

        for (Player p : Bukkit.getOnlinePlayers()) {
            
            List<String> groups = database.getInheritance(p.getUniqueId().toString());
            
            if (groups.contains(target)) {

                Map<String, ChangeType> eventMap = new HashMap<>();

                for (String s : perms.keySet()) {

                    eventMap.put(s, perms.get(s) ? TRUE : FALSE);

                }

                PermissionChangeEvent event = new PermissionChangeEvent(p, eventMap);
                Bukkit.getPluginManager().callEvent(event);

                if (!event.isCancelled()) {

                    PermissionAttachment a = attachments.get(p);

                    for (String s : perms.keySet()) {

                        a.setPermission(s, perms.get(s));

                    }

                    p.updateCommands();

                }
                
            }
            
        }
        
    }

    public void removeGroupParent(String target, String parent) throws DatabaseException {

        List<String> parents = getGroupInheritance(parent);

        Map<String, Boolean> perms = new HashMap<>();

        for (String s : parents) {

            perms.putAll(database.getPermissions(s));

        }

        perms.putAll(database.getPermissions(parent));

        for (Player p : Bukkit.getOnlinePlayers()) {

            List<String> groups = database.getInheritance(p.getUniqueId().toString());

            if (groups.contains(target)) {

                Map<String, ChangeType> eventMap = new HashMap<>();

                for (String s : perms.keySet()) {

                    eventMap.put(s, REMOVE);

                }

                PermissionChangeEvent event = new PermissionChangeEvent(p, eventMap);
                Bukkit.getPluginManager().callEvent(event);

                if (!event.isCancelled()) {

                    PermissionAttachment a = attachments.get(p);

                    for (String s : perms.keySet()) {

                        a.unsetPermission(s);

                    }

                    p.updateCommands();

                }

            }

        }

    }

    public void setGroupPrefix(String target, String prefix) throws DatabaseException {

        String oldPrefix = database.getPrefix(target);

        for (Player p : Bukkit.getOnlinePlayers()) {

            if (getPlayerPrefix(p) == null || getPlayerPrefix(p).equals(oldPrefix)) {

                if (database.getInheritance(p.getUniqueId().toString()).contains(target)) {

                    setPlayerPrefix(p, prefix);

                }

            }

        }

    }

    public void setGroupSuffix(String target, String suffix) throws DatabaseException {

        String oldSuffix = database.getSuffix(target);

        for (Player p : Bukkit.getOnlinePlayers()) {

            if (getPlayerSuffix(p) == null || getPlayerSuffix(p).equals(oldSuffix)) {

                if (database.getInheritance(p.getUniqueId().toString()).contains(target)) {

                    setPlayerSuffix(p, suffix);

                }

            }

        }

    }

    public void removeGroupPrefix(String target) throws DatabaseException {

        String prefix = database.getPrefix(target);

        if (prefix != null) {

            for (Player p : Bukkit.getOnlinePlayers()) {

                if (getPlayerPrefix(p).equals(prefix)) {

                    if (database.getInheritance(p.getUniqueId().toString()).contains(target)) {

                        removePlayerPrefix(p);

                    }

                }

            }

        }

    }

    public void removeGroupSuffix(String target) throws DatabaseException {

        String suffix = database.getSuffix(target);

        if (suffix != null) {

            for (Player p : Bukkit.getOnlinePlayers()) {

                if (getPlayerPrefix(p).equals(suffix)) {

                    if (database.getInheritance(p.getUniqueId().toString()).contains(target)) {

                        removePlayerPrefix(p);

                    }

                }

            }

        }

    }

    public void deletePlayer(Player target) {

        removePlayerPrefix(target);
        removePlayerSuffix(target);

        attachments.get(target).remove();
        attachments.remove(target);

        attachments.put(target, target.addAttachment(m));

        target.updateCommands();

    }

    public void handleGroupDeletion(String target) throws DatabaseException {

        Map<String, Boolean> perms = new HashMap<>();

        List<String> parents = getGroupInheritance(target);

        for (String s : parents) {

            perms.putAll(database.getPermissions(s));

        }

        perms.putAll(database.getPermissions(target));

        for (Player p : Bukkit.getOnlinePlayers()) {

            List<String> groups = database.getInheritance(p.getUniqueId().toString());

            List<String> inherited = new ArrayList<>();

            for (String s : groups) {

                inherited.addAll(getGroupInheritance(s));

            }

            groups.addAll(inherited);

            if (groups.contains(target)) {

                PermissionAttachment a = attachments.get(p);

                for (String s : perms.keySet()) {

                    a.unsetPermission(s);

                }

                p.updateCommands();

            }

        }

    }

    public void handleJoin(Player p) {

        PermissionAttachment a = p.addAttachment(m);

        attachments.put(p, a);

        try {

            String id;

            if (substitutions.containsKey(p)) {

               id = substitutions.get(p).getUniqueId().toString();

               a.setPermission("kxperms.command.su", true);

            } else {

                id = p.getUniqueId().toString();

            }

            Map<String, Boolean> perms = new HashMap<>();

            List<String> groups = database.getInheritance(id), parents = new ArrayList<>();

            for (String s : groups) {

                parents.addAll(getGroupInheritance(s));

            }

            for (String s : parents) {

                perms.putAll(database.getPermissions(s));

            }

            for (String s : groups) {

                perms.putAll(database.getPermissions(s));

            }

            perms.putAll(database.getPermissions(id));

            Map<String, ChangeType> eventMap = new HashMap<>();

            for (String s : perms.keySet()) {

                eventMap.put(s, perms.get(s) ? TRUE : FALSE);

            }

            PermissionChangeEvent event = new PermissionChangeEvent(p, eventMap);
            Bukkit.getPluginManager().callEvent(event);

            if (!event.isCancelled()) {

                for (String s : perms.keySet()) {

                    a.setPermission(s, perms.get(s));

                }

            }

            String prefix = database.getPrefix(id);

            if (prefix == null) {

                List<String> inheritance = database.getInheritance(p.getUniqueId().toString());

                if (!inheritance.isEmpty()) setPlayerPrefix(p, database.getPrefix(inheritance.get(0)));

            } else {

                setPlayerPrefix(p, prefix);

            }

            String suffix = database.getSuffix(id);

            if (suffix == null) {

                List<String> inheritance = database.getInheritance(p.getUniqueId().toString());

                if (!inheritance.isEmpty()) setPlayerSuffix(p, database.getSuffix(inheritance.get(0)));

            } else {

                setPlayerSuffix(p, suffix);

            }

        } catch (DatabaseException e) {

            System.err.println("A database error occurred while " + p.getName() + " was logging in.");
            e.printStackTrace();

        }

        p.updateCommands();

    }

    public void handleLeave(Player p) {

        PermissionAttachment a = attachments.get(p);

        if (a != null) a.remove();

        attachments.remove(p);

        removePlayerPrefix(p);
        removePlayerSuffix(p);

        removeSubstitute(p);

    }

    public void changeDatabase(PermissionsDatabase database) {

        this.database = database;

    }

    public void addSubstitute(Player p, OfflinePlayer substitute) {

        substitutions.put(p, substitute);

    }

    public void removeSubstitute(Player p) {

        substitutions.remove(p);

    }

    public boolean isSubstituting(Player p) {

        return substitutions.containsKey(p);

    }

    public void simulateRelog(Player p) {

        OfflinePlayer substitution = null;

        if (substitutions.containsKey(p)) {

            substitution = substitutions.get(p);

        }

        Bukkit.getPluginManager().callEvent(new PlayerQuitEvent(p, null));

        new BukkitRunnable() {

            @Override
            public void run() {

                Bukkit.getPluginManager().callEvent(new PlayerJoinEvent(p, null));

            }

        }.runTaskLater(m, 5L);

        if (substitution != null) {

            substitutions.put(p, substitution);

        }

    }

}

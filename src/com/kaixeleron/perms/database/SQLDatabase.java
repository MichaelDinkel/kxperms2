package com.kaixeleron.perms.database;

import java.sql.*;
import java.util.*;

public class SQLDatabase implements PermissionsDatabase {

    private final String host, database, username, password;
    private final int port;

    private Connection c = null;

    public SQLDatabase(final String host, final int port, final String database, final String username, final String password) throws SQLException {

        this.host = host;
        this.port = port;
        this.database = database;
        this.username = username;
        this.password = password;

        connect();

        PreparedStatement perms = null, groups = null, inheritance = null, chat = null, createDefault = null;

        try {

            perms = c.prepareStatement("CREATE TABLE IF NOT EXISTS `kxperms_perms` (entity VARCHAR(64), permission VARCHAR(128), value BIT);");
            groups = c.prepareStatement("CREATE TABLE IF NOT EXISTS `kxperms_groups` (name VARCHAR(64), PRIMARY KEY (name));");
            inheritance = c.prepareStatement("CREATE TABLE IF NOT EXISTS `kxperms_inheritance` (entity VARCHAR(64), parent VARCHAR(64));");
            chat = c.prepareStatement("CREATE TABLE IF NOT EXISTS `kxperms_chat` (entity VARCHAR(64), prefix VARCHAR(1024), suffix VARCHAR(1024), PRIMARY KEY (entity));");

            perms.executeUpdate();
            groups.executeUpdate();
            inheritance.executeUpdate();
            chat.executeUpdate();

            createDefault = c.prepareStatement("INSERT IGNORE INTO `kxperms_groups` (`name`) VALUES (?);");
            createDefault.setString(1, "default");
            createDefault.executeUpdate();

        } finally {

            closeSilently(perms, groups, inheritance, chat, createDefault);

        }
    }

    private void closeSilently(AutoCloseable... ac) {

        for (AutoCloseable a : ac) {

            try {

                if (a != null) a.close();

            } catch (Exception ignored) {}
        }
    }

    private void connect() throws SQLException {

        if (c == null || c.isClosed()) c = DriverManager.getConnection(String.format("jdbc:mysql://%s:%d/%s?useSSL=false", host, port, database), username, password);

    }

    @Override
    public void setPermission(String target, String permission, boolean value) throws DatabaseException {

        PreparedStatement get = null, set = null;
        ResultSet rs = null;

        try {

            connect();

            get = c.prepareStatement("SELECT * FROM `kxperms_perms` WHERE `entity` = ? AND `permission` = ?;");
            get.setString(1, target);
            get.setString(2, permission);
            rs = get.executeQuery();

            if (rs.next()) {

                set = c.prepareStatement("UPDATE `kxperms_perms` SET `value` = ? WHERE `entity` = ? AND `permission` = ?;");
                set.setBoolean(1, value);
                set.setString(2, target);
                set.setString(3, permission);
                set.executeUpdate();

            } else {

                set = c.prepareStatement("INSERT INTO `kxperms_perms` (entity, permission, value) VALUES (?, ?, ?);");
                set.setString(1, target);
                set.setString(2, permission);
                set.setBoolean(3, value);
                set.executeUpdate();

            }

        } catch (SQLException e) {

            throw new DatabaseException(e);

        } finally {

            closeSilently(get, set, rs);

        }

    }

    @Override
    public void removePermission(String target, String permission) throws DatabaseException {

        PreparedStatement ps = null;

        try {

            connect();

            ps = c.prepareStatement("DELETE FROM `kxperms_perms` WHERE `entity` = ? AND `permission` = ?;");
            ps.setString(1, target);
            ps.setString(2, permission);
            ps.executeUpdate();

        } catch (SQLException e) {

            throw new DatabaseException(e);

        } finally {

            closeSilently(ps);

        }

    }

    @Override
    public Map<String, Boolean> getPermissions(String target) throws DatabaseException {

        Map<String, Boolean> out = new HashMap<>();

        PreparedStatement get = null;
        ResultSet rs = null;

        try {

            connect();

            get = c.prepareStatement("SELECT `permission`,`value` FROM `kxperms_perms` WHERE `entity` = ?;");
            get.setString(1, target);
            rs = get.executeQuery();

            while (rs.next()) {

                out.put(rs.getString("permission"), rs.getBoolean("value"));

            }

        } catch (SQLException e) {

            throw new DatabaseException(e);

        } finally {

            closeSilently(get, rs);

        }

        return out;

    }

    @Override
    public void addGroup(String target, String group) throws DatabaseException {

        PreparedStatement insert = null;

        try {

            connect();

            insert = c.prepareStatement("INSERT INTO `kxperms_inheritance` (`entity`, `parent`) VALUES (?, ?);");
            insert.setString(1, target);
            insert.setString(2, group);
            insert.executeUpdate();

        } catch (SQLException e) {

            throw new DatabaseException(e);

        } finally {

            closeSilently(insert);

        }

    }

    @Override
    public void removeGroup(String target, String group) throws DatabaseException {

        PreparedStatement delete = null;

        try {

            connect();

            delete = c.prepareStatement("DELETE FROM `kxperms_inheritance` WHERE `entity` = ? AND `parent` = ?;");
            delete.setString(1, target);
            delete.setString(2, group);
            delete.executeUpdate();

        } catch (SQLException e) {

            throw new DatabaseException(e);

        } finally {

            closeSilently(delete);

        }

    }

    @Override
    public List<String> getInheritance(String target) throws DatabaseException {

        if (target.equals("default")) return new ArrayList<>();

        List<String> out = new ArrayList<>();

        PreparedStatement get = null;
        ResultSet rs = null;

        try {

            connect();

            get = c.prepareStatement("SELECT `parent` FROM `kxperms_inheritance` WHERE `entity` = ?;");
            get.setString(1, target);
            rs = get.executeQuery();

            while (rs.next()) {

                String parent = rs.getString("parent");

                if (!out.contains(parent)) out.add(parent);

            }

        } catch (SQLException e) {

            throw new DatabaseException(e);

        } finally {

            closeSilently(get, rs);

        }

        if (!out.contains("default")) out.add("default");

        return out;

    }

    @Override
    public void createGroup(String target) throws DatabaseException {

        PreparedStatement create = null;

        try {

            connect();

            create = c.prepareStatement("INSERT INTO `kxperms_groups` (name) VALUES (?);");
            create.setString(1, target);
            create.executeUpdate();

        } catch (SQLException e) {

            throw new DatabaseException(e);

        } finally {

            closeSilently(create);

        }

    }

    @Override
    public void deleteGroup(String target) throws DatabaseException {

        PreparedStatement delete = null, inheritance = null, permissions = null, chat = null;

        try {

            connect();

            delete = c.prepareStatement("DELETE FROM `kxperms_groups` WHERE `name` = ?;");
            delete.setString(1, target);
            delete.executeUpdate();

            inheritance = c.prepareStatement("DELETE FROM `kxperms_inheritance` WHERE `entity` = ? OR `parent` = ?;");
            inheritance.setString(1, target);
            inheritance.setString(2, target);
            inheritance.executeUpdate();

            permissions = c.prepareStatement("DELETE FROM `kxperms_perms` WHERE `entity` = ?;");
            permissions.setString(1, target);
            permissions.executeUpdate();

            chat = c.prepareStatement("DELETE FROM `kxperms_chat` WHERE `entity` = ?;");
            chat.setString(1, target);
            chat.executeUpdate();

        } catch (SQLException e) {

            throw new DatabaseException(e);

        } finally {

            closeSilently(delete, inheritance, permissions, chat);

        }

    }

    @Override
    public List<String> listGroups() throws DatabaseException {

        List<String> out = new ArrayList<>();

        PreparedStatement get = null;
        ResultSet rs = null;

        try {

            connect();

            get = c.prepareStatement("SELECT * FROM `kxperms_groups`;");
            rs = get.executeQuery();

            while (rs.next()) {

                out.add(rs.getString("name"));

            }

        } catch (SQLException e) {

            throw new DatabaseException(e);

        } finally {

            closeSilently(get, rs);

        }

        return out;

    }

    @Override
    public void setPrefix(String target, String prefix) throws DatabaseException {

        PreparedStatement set = null;

        try {

            connect();

            set = c.prepareStatement("INSERT INTO `kxperms_chat` (entity, prefix, suffix) VALUES (?, ?,?) ON DUPLICATE KEY UPDATE `prefix` = ?;");
            set.setString(1, target);
            set.setString(2, prefix);
            set.setString(3, "");
            set.setString(4, prefix);
            set.executeUpdate();

        } catch (SQLException e) {

            throw new DatabaseException(e);

        } finally {

            closeSilently(set);

        }

    }

    @Override
    public void setSuffix(String target, String suffix) throws DatabaseException {

        PreparedStatement set = null;

        try {

            connect();

            set = c.prepareStatement("INSERT INTO `kxperms_chat` (entity, prefix, suffix) VALUES (?, ?,?) ON DUPLICATE KEY UPDATE `suffix` = ?;");
            set.setString(1, target);
            set.setString(2, "");
            set.setString(3, suffix);
            set.setString(4, suffix);
            set.executeUpdate();

        } catch (SQLException e) {

            throw new DatabaseException(e);

        } finally {

            closeSilently(set);

        }

    }

    @Override
    public void removePrefix(String target) throws DatabaseException {

        PreparedStatement check = null, update = null;
        ResultSet rs = null;

        try {

            connect();

            check = c.prepareStatement("SELECT `suffix` FROM `kxperms_chat` WHERE `entity` = ?;");
            check.setString(1, target);
            rs = check.executeQuery();

            if (rs.next()) {

                if (rs.getString("suffix") == null) {

                    update = c.prepareStatement("DELETE FROM `kxperms_chat` WHERE `entity` = ?;");
                    update.setString(1, target);
                    update.executeUpdate();

                } else {

                    update = c.prepareStatement("UPDATE `kxperms_chat` SET `prefix` = NULL WHERE `entity` = ?;");
                    update.setString(1, target);
                    update.executeUpdate();

                }

            } else {

                update = c.prepareStatement("DELETE FROM `kxperms_chat` WHERE `entity` = ?;");
                update.setString(1, target);
                update.executeUpdate();

            }

        } catch (SQLException e) {

            throw new DatabaseException(e);

        } finally {

            closeSilently(check, update, rs);

        }

    }

    @Override
    public void removeSuffix(String target) throws DatabaseException {

        PreparedStatement check = null, update = null;
        ResultSet rs = null;

        try {

            connect();

            check = c.prepareStatement("SELECT `prefix` FROM `kxperms_chat` WHERE `entity` = ?;");
            check.setString(1, target);
            rs = check.executeQuery();

            if (rs.next()) {

                if (rs.getString("prefix") == null) {

                    update = c.prepareStatement("DELETE FROM `kxperms_chat` WHERE `entity` = ?;");
                    update.setString(1, target);
                    update.executeUpdate();

                } else {

                    update = c.prepareStatement("UPDATE `kxperms_chat` SET `suffix` = NULL WHERE `entity` = ?;");
                    update.setString(1, target);
                    update.executeUpdate();

                }

            } else {

                update = c.prepareStatement("DELETE FROM `kxperms_chat` WHERE `entity` = ?;");
                update.setString(1, target);
                update.executeUpdate();

            }

        } catch (SQLException e) {

            throw new DatabaseException(e);

        } finally {

            closeSilently(check, update, rs);

        }

    }

    @Override
    public String getPrefix(String target) throws DatabaseException {

        String out = null;

        PreparedStatement get = null;
        ResultSet rs = null;

        try {

            connect();

            get = c.prepareStatement("SELECT `prefix` FROM `kxperms_chat` WHERE `entity` = ?;");
            get.setString(1, target);
            rs = get.executeQuery();

            if (rs.next()) {

                out = rs.getString("prefix");

            }

        } catch (SQLException e) {

            throw new DatabaseException(e);

        } finally {

            closeSilently(get, rs);

        }

        return out;

    }

    @Override
    public String getSuffix(String target) throws DatabaseException {

        String out = null;

        PreparedStatement get = null;
        ResultSet rs = null;

        try {

            connect();

            get = c.prepareStatement("SELECT `suffix` FROM `kxperms_chat` WHERE `entity` = ?;");
            get.setString(1, target);
            rs = get.executeQuery();

            if (rs.next()) {

                out = rs.getString("suffix");

            }

        } catch (SQLException e) {

            throw new DatabaseException(e);

        } finally {

            closeSilently(get, rs);

        }

        return out;

    }

    @Override
    public void deletePlayer(String target) throws DatabaseException {

        PreparedStatement deletePerms = null, deleteChat = null, deleteInheritance = null;

        try {

            connect();

            deletePerms = c.prepareStatement("DELETE FROM `kxperms_permissions` WHERE `entity` = ?;");
            deletePerms.setString(1, target);

            deleteChat = c.prepareStatement("DELETE FROM `kxperms_chat` WHERE `entity` = ?;");
            deleteChat.setString(1, target);

            deleteInheritance = c.prepareStatement("DELETE FROM `kxperms_inheritance` WHERE `entity` = ?;");
            deleteInheritance.setString(1, target);

            deletePerms.executeUpdate();
            deleteChat.executeUpdate();
            deleteInheritance.executeUpdate();

        } catch (SQLException e) {

            throw new DatabaseException(e);

        } finally {

            closeSilently(deletePerms, deleteChat, deleteInheritance);

        }

    }

    @Override
    public void close() {

        closeSilently(c);

    }

}

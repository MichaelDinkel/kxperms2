package com.kaixeleron.perms.database;

public class DatabaseException extends Exception {

    private final Exception parent;

    DatabaseException(Exception parent) {

        this.parent = parent;

    }

    @Override
    public void printStackTrace() {

        parent.printStackTrace();
        super.printStackTrace();

    }

}

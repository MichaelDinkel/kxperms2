package com.kaixeleron.perms.database;

import com.kaixeleron.perms.PermissionsMain;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class YAMLDatabase implements PermissionsDatabase {

    private final File permsFile;
    private FileConfiguration perms;

    public YAMLDatabase(PermissionsMain m) throws IOException {

        permsFile = new File(m.getDataFolder(), "permissions.yml");

        if (!permsFile.exists()) //noinspection ResultOfMethodCallIgnored
            permsFile.createNewFile();

        reload();

    }

    private void reload() {

        perms = YamlConfiguration.loadConfiguration(permsFile);

    }

    private void save() throws DatabaseException {

        try {

            perms.save(permsFile);

        } catch (IOException e) {

            throw new DatabaseException(e);

        }

    }

    @Override
    public void setPermission(String target, String permission, boolean value) throws DatabaseException {

        List<String> permList = perms.getStringList(String.format("%s.permissions.%s", target, value ? "granted" : "negated"));

        if (!permList.contains(permission)) permList.add(permission);

        List<String> otherList = perms.getStringList(String.format("%s.permissions.%s", target, value ? "negated" : "granted"));

        if (otherList.contains(permission)) {

            otherList.remove(permission);
            perms.set(String.format("%s.permissions.%s", target, value ? "negated" : "granted"), otherList);

        }

        perms.set(String.format("%s.permissions.%s", target, value ? "granted" : "negated"), permList);

        save();

    }

    @Override
    public void removePermission(String target, String permission) throws DatabaseException {

        List<String> grantedList = perms.getStringList(String.format("%s.permissions.granted", target));

        grantedList.remove(permission);

        List<String> negatedList = perms.getStringList(String.format("%s.permissions.negated", target));

        negatedList.remove(permission);

        perms.set(String.format("%s.permissions.granted", target), grantedList);
        perms.set(String.format("%s.permissions.negated", target), negatedList);

        save();

    }

    @Override
    public Map<String, Boolean> getPermissions(String target) {

        Map<String, Boolean> out = new HashMap<>();

        List<String> grantedList = perms.getStringList(String.format("%s.permissions.granted", target));

        for (String s : grantedList) {

            out.put(s, true);

        }

        List<String> negatedList = perms.getStringList(String.format("%s.permissions.negated", target));

        for (String s : negatedList) {

            out.put(s, false);

        }

        return out;

    }

    @Override
    public void addGroup(String target, String group) throws DatabaseException {

        List<String> inheritance = perms.getStringList(String.format("%s.inheritance", target));

        if (!inheritance.contains(group)) inheritance.add(group);

        perms.set(String.format("%s.inheritance", target), inheritance);
        save();

    }

    @Override
    public void removeGroup(String target, String group) throws DatabaseException {

        List<String> inheritance = perms.getStringList(String.format("%s.inheritance", target));

        inheritance.remove(group);

        perms.set(String.format("%s.inheritance", target), inheritance);
        save();

    }

    @Override
    public List<String> getInheritance(String target) {

        if (target.equals("default")) return new ArrayList<>();

        List<String> inheritance = perms.getStringList(String.format("%s.inheritance", target));

        if (!inheritance.contains("default")) inheritance.add("default");

        return inheritance;

    }

    @Override
    public void createGroup(String target) throws DatabaseException {

        List<String> groupList = perms.getStringList("groups");

        if (!groupList.contains(target)) groupList.add(target);

        perms.set("groups", groupList);

        save();

    }

    @Override
    public void deleteGroup(String target) throws DatabaseException {

        List<String> groupList = perms.getStringList("groups");

        groupList.remove(target);

        perms.set("groups", groupList);

        for (String s : perms.getKeys(false)) {

            List<String> inheritance = perms.getStringList(String.format("%s.inheritance", s));

            if (inheritance.contains(s)) {

                inheritance.remove(s);
                perms.set(String.format("%s.inheritance", s), inheritance);

            }

        }

        perms.set(target, null);

        save();

    }

    @Override
    public List<String> listGroups() {

        return perms.getStringList("groups");

    }

    @Override
    public void setPrefix(String target, String prefix) throws DatabaseException {

        perms.set(String.format("%s.prefix", target), prefix);
        save();

    }

    @Override
    public void setSuffix(String target, String suffix) throws DatabaseException {

        perms.set(String.format("%s.suffix", target), suffix);
        save();

    }

    @Override
    public void removePrefix(String target) throws DatabaseException {

        perms.set(String.format("%s.prefix", target), null);
        save();

    }

    @Override
    public void removeSuffix(String target) throws DatabaseException {

        perms.set(String.format("%s.suffix", target), null);
        save();

    }

    @Override
    public String getPrefix(String target) {

        return perms.getString(String.format("%s.prefix", target));

    }

    @Override
    public String getSuffix(String target) {

        return perms.getString(String.format("%s.suffix", target));

    }

    @Override
    public void deletePlayer(String target) throws DatabaseException {

        perms.set(target, null);
        save();

    }

    @Override
    public void close() {}

}

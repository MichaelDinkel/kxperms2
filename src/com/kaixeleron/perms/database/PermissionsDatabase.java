package com.kaixeleron.perms.database;

import java.util.Map;
import java.util.List;

public interface PermissionsDatabase {

    void setPermission(String target, String permission, boolean value) throws DatabaseException;

    void removePermission(String target, String permission) throws DatabaseException;

    Map<String, Boolean> getPermissions(String target) throws DatabaseException;

    void addGroup(String target, String group) throws DatabaseException;

    void removeGroup(String target, String group) throws DatabaseException;

    List<String> getInheritance(String target) throws DatabaseException;

    void createGroup(String target) throws DatabaseException;

    void deleteGroup(String target) throws DatabaseException;

    List<String> listGroups() throws DatabaseException;

    void setPrefix(String target, String prefix) throws DatabaseException;

    void setSuffix(String target, String suffix) throws DatabaseException;

    void removePrefix(String target) throws DatabaseException;

    void removeSuffix(String target) throws DatabaseException;

    String getPrefix(String target) throws DatabaseException;

    String getSuffix(String target) throws DatabaseException;

    void deletePlayer(String target) throws DatabaseException;

    void close();

}

package com.kaixeleron.perms.listener;

import com.kaixeleron.perms.permission.PermissionManager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class ConnectListener implements Listener {

    private final PermissionManager manager;

    public ConnectListener(PermissionManager manager) {

        this.manager = manager;

    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onJoin(PlayerJoinEvent event) {

        manager.handleJoin(event.getPlayer());

    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {

        manager.handleLeave(event.getPlayer());

    }

    @EventHandler
    public void onKick(PlayerKickEvent event) {

        manager.handleLeave(event.getPlayer());

    }

}

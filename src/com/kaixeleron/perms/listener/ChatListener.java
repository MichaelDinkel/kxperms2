package com.kaixeleron.perms.listener;

import com.kaixeleron.perms.permission.PermissionManager;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class ChatListener implements Listener {

    private final PermissionManager manager;

    private final String format;

    public ChatListener(PermissionManager manager, String format) {

        this.manager = manager;

        this.format = format;

    }

    @EventHandler
    public void onChat(AsyncPlayerChatEvent event) {

        String prefix = manager.getPlayerPrefix(event.getPlayer()), suffix = manager.getPlayerSuffix(event.getPlayer());

        if (prefix == null) prefix = "";
        if (suffix == null) suffix = "";

        event.setFormat(format.replace("$prefix", ChatColor.translateAlternateColorCodes('&', prefix)).replace("$suffix", ChatColor.translateAlternateColorCodes('&', suffix)).replace("$name", "%1$s").replace("$message", "%2$s"));

    }

}

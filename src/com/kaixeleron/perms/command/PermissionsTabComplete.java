package com.kaixeleron.perms.command;

import com.kaixeleron.perms.database.DatabaseException;
import com.kaixeleron.perms.database.PermissionsDatabase;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;
import org.bukkit.permissions.Permission;

import java.util.ArrayList;
import java.util.List;

public class PermissionsTabComplete implements TabCompleter {

    private PermissionsDatabase database;

    public PermissionsTabComplete(PermissionsDatabase database) {

        this.database = database;

    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {

        List<String> out = new ArrayList<>();

        switch (args.length) {

            case 1:

                if (args[0].length() > 0) {

                    String arg = args[0].toLowerCase();

                    if ("user".startsWith(arg)) out.add("user");
                    if ("group".startsWith(arg)) out.add("group");

                } else {

                    out.add("user");
                    out.add("group");

                }

                break;

            case 2:

                if (args[0].equalsIgnoreCase("user")) {

                    if (args[1].length() > 0) {

                        for (Player p : Bukkit.getOnlinePlayers()) {

                            if (p.getName().toLowerCase().startsWith(args[1].toLowerCase())) out.add(p.getName());

                        }

                    } else {

                        for (Player p : Bukkit.getOnlinePlayers()) {

                            out.add(p.getName());

                        }

                    }

                } else if (args[0].equalsIgnoreCase("group")) {

                    try {

                        if (args[1].length() > 0) {

                            List<String> groups = database.listGroups();
                            groups.add("list");

                            String arg = args[1].toLowerCase();

                            for (String s : groups) {

                                if (s.startsWith(arg)) out.add(s);

                            }

                        } else {

                            out.addAll(database.listGroups());
                            out.add("list");

                        }

                    } catch (DatabaseException e) {

                        e.printStackTrace();

                    }

                }

                break;

            case 3:

                if (args[0].equalsIgnoreCase("user")) {

                    if (args[2].length() > 0) {

                        String arg = args[2].toLowerCase();

                        if ("perm".startsWith(arg)) out.add("perm");
                        if ("group".startsWith(arg)) out.add("group");
                        if ("prefix".startsWith(arg)) out.add("prefix");
                        if ("suffix".startsWith(arg)) out.add("suffix");
                        if ("delete".startsWith(arg)) out.add("delete");

                    } else {

                        out.add("perm");
                        out.add("group");
                        out.add("prefix");
                        out.add("suffix");
                        out.add("delete");

                    }

                } else if (args[0].equalsIgnoreCase("group")) {

                    if (args[2].length() > 0) {

                        String arg = args[2].toLowerCase();

                        if ("perm".startsWith(arg)) out.add("perm");
                        if ("parent".startsWith(arg)) out.add("parent");
                        if ("prefix".startsWith(arg)) out.add("prefix");
                        if ("suffix".startsWith(arg)) out.add("suffix");
                        if ("create".startsWith(arg)) out.add("create");
                        if ("delete".startsWith(arg)) out.add("delete");

                    } else {

                        out.add("perm");
                        out.add("parent");
                        out.add("prefix");
                        out.add("suffix");
                        out.add("create");
                        out.add("delete");

                    }

                }

                break;

            case 4:

                switch (args[2].toLowerCase()) {

                    case "prefix":
                    case "suffix":

                        if (args[3].length() > 0) {

                            String arg = args[3].toLowerCase();

                            if ("view".startsWith(arg)) out.add("view");

                        } else {

                            out.add("view");

                        }

                    case "group":
                    case "parent":

                        if (args[3].length() > 0) {

                            String arg = args[3].toLowerCase();

                            if ("add".startsWith(arg)) out.add("add");
                            if ("remove".startsWith(arg)) out.add("remove");

                        } else {

                            out.add("add");
                            out.add("remove");

                        }

                        break;

                    case "perm":

                        if (args[3].length() > 0) {

                            String arg = args[3].toLowerCase();

                            if ("add".startsWith(arg)) out.add("add");
                            if ("remove".startsWith(arg)) out.add("remove");
                            if ("negate".startsWith(arg)) out.add("negate");

                        } else {

                            out.add("add");
                            out.add("remove");
                            out.add("negate");

                        }

                        break;

                }

                break;

            case 5:

                switch (args[2].toLowerCase()) {

                    case "group":

                        switch (args[3].toLowerCase()) {

                            case "add":
                            case "remove":

                                if (args[4].length() > 0) {

                                    String arg = args[4].toLowerCase();

                                    try {

                                        for (String s : database.listGroups()) {

                                            if (s.startsWith(arg)) out.add(s);

                                        }

                                    } catch (DatabaseException e) {

                                        e.printStackTrace();

                                    }

                                } else {

                                    try {

                                        out.addAll(database.listGroups());

                                    } catch (DatabaseException e) {

                                        e.printStackTrace();

                                    }

                                }

                                break;

                        }

                        break;

                    case "parent":

                        switch (args[3].toLowerCase()) {

                            case "add":

                                if (args[4].length() > 0) {

                                    String arg = args[4].toLowerCase();

                                    try {

                                        List<String> parents = database.getInheritance(args[1]);

                                        parents.remove(args[1]);

                                        for (String s : database.listGroups()) {

                                            if (s.startsWith(arg) && !parents.contains(s)) out.add(s);

                                        }

                                    } catch (DatabaseException e) {

                                        e.printStackTrace();

                                    }

                                } else {

                                    try {

                                        out.addAll(database.listGroups());

                                        out.remove(args[1]);

                                        for (String s : database.getInheritance(args[1])) {

                                            if (out.contains(s)) out.remove(s);

                                        }

                                    } catch (DatabaseException e) {

                                        e.printStackTrace();

                                    }

                                }

                                break;

                            case "remove":

                                if (args[4].length() > 0) {

                                    String arg = args[4].toLowerCase();

                                    try {

                                        for (String s : database.getInheritance(args[1])) {

                                            if (s.startsWith(arg)) out.add(s);

                                        }

                                    } catch (DatabaseException e) {

                                        e.printStackTrace();

                                    }

                                } else {

                                    try {

                                        out.addAll(database.getInheritance(args[1]));

                                    } catch (DatabaseException e) {

                                        e.printStackTrace();

                                    }

                                }

                                break;

                        }

                        break;

                    case "perm":

                        switch (args[3].toLowerCase()) {

                            case "add":
                            case "negate":

                                if (args[4].length() > 0) {

                                    String arg = args[4].toLowerCase();

                                    for (Permission p : Bukkit.getPluginManager().getPermissions()) {

                                        if (p.getName().startsWith(arg)) out.add(p.getName());

                                    }

                                } else {

                                    for (Permission p : Bukkit.getPluginManager().getPermissions()) {

                                        out.add(p.getName());

                                    }

                                }

                                break;

                            case "remove":

                                switch (args[0].toLowerCase()) {

                                    case "user":

                                        @SuppressWarnings("deprecation") OfflinePlayer p = Bukkit.getOfflinePlayer(args[0]);

                                        if (args[4].length() > 0) {

                                            String arg = args[4].toLowerCase();

                                            try {

                                                for (String s : database.getPermissions(p.getUniqueId().toString()).keySet()) {

                                                    if (s.startsWith(arg)) out.add(s);

                                                }

                                            } catch (DatabaseException e) {

                                                e.printStackTrace();

                                            }

                                        } else {

                                            try {

                                                out.addAll(database.getPermissions(p.getUniqueId().toString()).keySet());

                                            } catch (DatabaseException e) {

                                                e.printStackTrace();

                                            }

                                        }

                                        break;

                                    case "group":

                                        if (args[4].length() > 0) {

                                            String arg = args[4].toLowerCase();

                                            try {

                                                for (String s : database.getPermissions(args[0]).keySet()) {

                                                    if (s.startsWith(arg)) out.add(s);

                                                }

                                            } catch (DatabaseException e) {

                                                e.printStackTrace();

                                            }

                                        } else {

                                            try {

                                                out.addAll(database.getPermissions(args[0]).keySet());

                                            } catch (DatabaseException e) {

                                                e.printStackTrace();

                                            }

                                        }

                                        break;

                                }

                                break;

                        }

                        break;

                }

                break;

        }

        return out;

    }

    public void changeDatabase(PermissionsDatabase database) {

        this.database = database;

    }

}

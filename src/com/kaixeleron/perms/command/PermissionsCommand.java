package com.kaixeleron.perms.command;

import com.kaixeleron.perms.database.DatabaseException;
import com.kaixeleron.perms.database.PermissionsDatabase;
import com.kaixeleron.perms.permission.PermissionManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import javax.annotation.Nullable;
import java.util.Map;
import java.util.List;

public class PermissionsCommand implements CommandExecutor {

    private PermissionsDatabase database;

    private final PermissionManager manager;

    public PermissionsCommand(PermissionsDatabase database, PermissionManager manager) {

        this.database = database;

        this.manager = manager;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (args.length < 2) {

            sender.sendMessage("Manage server permissions.");
            sender.sendMessage(ChatColor.RED + "Usage: " + ChatColor.RESET + "/" + label + " <user|group> <name|list> <perm|group|parent|prefix|suffix|create|delete> <add|remove|negate> <value>");

        } else if (args.length == 2) {

            switch (args[0].toLowerCase()) {

                case "user":

                    showPlayer(sender, args[1]);

                    break;

                case "group":

                    if (args[1].equalsIgnoreCase("list")) {

                        listGroups(sender);

                    } else {

                        showGroup(sender, args[1]);

                    }

                    break;

                default:

                    sender.sendMessage(ChatColor.RED + "Invalid target type. Must be " + ChatColor.RESET + "user" + ChatColor.RED + " or " + ChatColor.RESET + "group" + ChatColor.RED + ".");

                    break;

            }

        } else if (args.length <= 4) {

            switch (args[0].toLowerCase()) {

                case "user":

                    switch (args[2].toLowerCase()) {

                        case "perm":

                            setUserPerm(sender, args[1], null, "");

                            break;

                        case "group":

                            setUserGroup(sender, args[1], null, "");

                            break;

                        case "prefix":

                            setUserPrefix(sender, args[1], null, "view");

                            break;

                        case "suffix":

                            setUserSuffix(sender, args[1], null, "view");

                            break;

                        case "delete":

                            deleteUser(sender, args[1]);

                            break;

                        default:

                            sender.sendMessage(ChatColor.RED + "Invalid operation. Must be " + ChatColor.RESET + "perm" + ChatColor.RED + ", " + ChatColor.RESET + "group" + ChatColor.RED + ", " + ChatColor.RESET + "prefix" + ChatColor.RED + ", " + ChatColor.RESET + "suffix" + ChatColor.RED + " or " + ChatColor.RESET + "delete" + ChatColor.RED + ".");

                            break;

                    }

                    break;

                case "group":

                    switch (args[2].toLowerCase()) {

                        case "perm":

                            setGroupPerm(sender, args[1], null, "");

                            break;

                        case "parent":

                            setGroupParent(sender, args[1], null, "");

                            break;

                        case "prefix":

                            setGroupPrefix(sender, args[1], null, "view");

                            break;

                        case "suffix":

                            setGroupSuffix(sender, args[1], null, "view");

                            break;

                        case "create":

                            createGroup(sender, args[1]);

                            break;

                        case "delete":

                            deleteGroup(sender, args[1]);

                            break;

                        default:

                            sender.sendMessage(ChatColor.RED + "Invalid operation. Must be " + ChatColor.RESET + "perm" + ChatColor.RED + ", " + ChatColor.RESET + "parent" + ChatColor.RED + ", " + ChatColor.RESET + "prefix" + ChatColor.RED + ", " + ChatColor.RESET + "suffix" + ChatColor.RED + ", " + ChatColor.WHITE + "create" + ChatColor.RED + ", or " + ChatColor.RESET + "delete" + ChatColor.RED + ".");

                            break;

                    }

                    break;

                default:

                    sender.sendMessage(ChatColor.RED + "Invalid target type. Must be " + ChatColor.RESET + "user" + ChatColor.RED + " or " + ChatColor.RESET + "group" + ChatColor.RED + ".");

                    break;

            }

        } else {

            switch (args[0].toLowerCase()) {

                case "user":

                    switch (args[2].toLowerCase()) {

                        case "perm":

                            setUserPerm(sender, args[1], args[4], args[3]);

                            break;

                        case "group":

                            setUserGroup(sender, args[1], args[4], args[3]);

                            break;

                        case "prefix":

                            StringBuilder prefix = new StringBuilder();

                            for (int i = 4; i < args.length; i++) {

                                prefix.append(args[i]).append(" ");

                            }

                            setUserPrefix(sender, args[1], prefix.toString().substring(0, prefix.length() - 1), args[3]);

                            break;

                        case "suffix":

                            StringBuilder suffix = new StringBuilder();

                            for (int i = 4; i < args.length; i++) {

                                suffix.append(args[i]).append(" ");

                            }

                            setUserSuffix(sender, args[1], suffix.toString().substring(0, suffix.length() - 1), args[3]);

                            break;

                        case "delete":

                            deleteUser(sender, args[1]);

                            break;

                        default:

                            sender.sendMessage(ChatColor.RED + "Invalid operation. Must be " + ChatColor.RESET + "perm" + ChatColor.RED + ", " + ChatColor.RESET + "group" + ChatColor.RED + ", " + ChatColor.RESET + "prefix" + ChatColor.RED + ", " + ChatColor.RESET + "suffix" + ChatColor.RED + " or " + ChatColor.RESET + "delete" + ChatColor.RED + ".");

                            break;

                    }

                    break;

                case "group":

                    switch (args[2].toLowerCase()) {

                        case "perm":

                            setGroupPerm(sender, args[1], args[4], args[3]);

                            break;

                        case "parent":

                            setGroupParent(sender, args[1], args[4], args[3]);

                            break;

                        case "prefix":

                            StringBuilder prefix = new StringBuilder();

                            for (int i = 4; i < args.length; i++) {

                                prefix.append(args[i]).append(" ");

                            }

                            setGroupPrefix(sender, args[1], prefix.toString().substring(0, prefix.length() - 1), args[3]);

                            break;

                        case "suffix":

                            StringBuilder suffix = new StringBuilder();

                            for (int i = 4; i < args.length; i++) {

                                suffix.append(args[i]).append(" ");

                            }

                            setGroupSuffix(sender, args[1], suffix.toString().substring(0, suffix.length() - 1), args[3]);

                            break;

                        case "create":

                            createGroup(sender, args[1]);

                            break;

                        case "delete":

                            deleteGroup(sender, args[1]);

                            break;

                        default:

                            sender.sendMessage(ChatColor.RED + "Invalid operation. Must be " + ChatColor.RESET + "perm" + ChatColor.RED + ", " + ChatColor.RESET + "parent" + ChatColor.RED + ", " + ChatColor.RESET + "prefix" + ChatColor.RED + ", " + ChatColor.RESET + "suffix" + ChatColor.RED + ", " + ChatColor.WHITE + "create" + ChatColor.RED + ", or " + ChatColor.RESET + "delete" + ChatColor.RED + ".");

                            break;

                    }

                    break;

                default:

                    sender.sendMessage(ChatColor.RED + "Invalid target type. Must be " + ChatColor.RESET + "user" + ChatColor.RED + " or " + ChatColor.RESET + "group" + ChatColor.RED + ".");

                    break;
            }

        }

        return true;

    }

    private void showPlayer(CommandSender sender, String target) {

        if (sender.hasPermission("kxperms.command.showplayer")) {

            try {

                @SuppressWarnings("deprecation") OfflinePlayer p = Bukkit.getOfflinePlayer(target);

                String id = p.getUniqueId().toString();

                Map<String, Boolean> perms = database.getPermissions(id);
                List<String> groups = database.getInheritance(id);
                String prefix = database.getPrefix(id);
                String suffix = database.getSuffix(id);

                if (perms.isEmpty() && groups.isEmpty() && prefix == null && suffix == null) {

                    sender.sendMessage("There is no permission data for user " + ChatColor.GRAY + target + ChatColor.RESET + ".");

                } else {

                    sender.sendMessage("Permission data for user " + ChatColor.GRAY + target + ChatColor.RESET + ":");

                    if (!perms.isEmpty()) {

                        sender.sendMessage("Permissions:");

                        for (String s : perms.keySet()) {

                            boolean value = perms.get(s);

                            sender.sendMessage(ChatColor.YELLOW + " - " + ChatColor.RESET + s + ": " + (value ? ChatColor.GREEN + "true" : ChatColor.RED + "false"));

                        }

                    }

                    if (!groups.isEmpty()) {

                        sender.sendMessage("Groups:");

                        for (String s : groups) {

                            sender.sendMessage(ChatColor.AQUA + " - " + ChatColor.RESET + s);

                        }

                    }

                    if (prefix != null) sender.sendMessage("Prefix: " + prefix);
                    if (suffix != null) sender.sendMessage("Suffix: " + suffix);

                }

            } catch (DatabaseException e) {

                sender.sendMessage(ChatColor.RED + "A database exception has occurred.");

                e.printStackTrace();

            }

        } else {

            sender.sendMessage(ChatColor.RED + "Access denied.");

        }

    }

    private void showGroup(CommandSender sender, String target) {

        if (sender.hasPermission("kxperms.command.showgroup")) {

            try {

                if (database.listGroups().contains(target)) {

                    Map<String, Boolean> perms = database.getPermissions(target);
                    List<String> groups = database.getInheritance(target);
                    String prefix = database.getPrefix(target);
                    String suffix = database.getSuffix(target);

                    if (perms.isEmpty() && groups.isEmpty() && prefix == null && suffix == null) {

                        sender.sendMessage("There is no permission data for group " + ChatColor.GRAY + target + ChatColor.RESET + ".");

                    } else {

                        sender.sendMessage("Permission data for group " + ChatColor.GRAY + target + ChatColor.RESET + ":");

                        if (!perms.isEmpty()) {

                            sender.sendMessage("Permissions:");

                            for (String s : perms.keySet()) {

                                boolean value = perms.get(s);

                                sender.sendMessage(ChatColor.YELLOW + " - " + ChatColor.RESET + s + ": " + (value ? ChatColor.GREEN + "true" : ChatColor.RED + "false"));

                            }

                        }

                        if (!groups.isEmpty()) {

                            sender.sendMessage("Parents:");

                            for (String s : groups) {

                                sender.sendMessage(ChatColor.AQUA + " - " + ChatColor.RESET + s);

                            }

                        }

                        if (prefix != null) sender.sendMessage("Prefix: " + prefix);
                        if (suffix != null) sender.sendMessage("Suffix: " + suffix);

                    }

                } else {

                    sender.sendMessage(ChatColor.RED + "Group " + ChatColor.RESET + target + ChatColor.RED + " does not exist.");

                }

            } catch (DatabaseException e) {

                sender.sendMessage(ChatColor.RED + "A database exception has occurred.");

                e.printStackTrace();

            }

        } else {

            sender.sendMessage(ChatColor.RED + "Access denied.");

        }

    }

    private void listGroups(CommandSender sender) {

        if (sender.hasPermission("kxperms.command.listgroups")) {

            try {

                List<String> groups = database.listGroups();

                if (groups.size() == 0) {

                    sender.sendMessage("There are no groups.");

                } else {

                    StringBuilder groupString = new StringBuilder();

                    for (String s : groups) groupString.append(s).append(", ");

                    sender.sendMessage("Active groups: " + groupString.substring(0, groupString.length() - 2));

                }

            } catch (DatabaseException e) {

                sender.sendMessage(ChatColor.RED + "A database exception has occurred.");

                e.printStackTrace();

            }

        } else {

            sender.sendMessage(ChatColor.RED + "Access denied.");

        }

    }

    private void setUserPerm(CommandSender sender, String target, String perm, String operation) {

        if (sender.hasPermission("kxperms.command.setuserperm")) {

            if (perm == null) {

                sender.sendMessage(ChatColor.RED + "You must specify a permission to set.");

            } else {

                @SuppressWarnings("deprecation") OfflinePlayer p = Bukkit.getOfflinePlayer(target);

                try {

                    switch (operation.toLowerCase()) {

                        case "add":

                            database.setPermission(p.getUniqueId().toString(), perm, true);

                            if (p.isOnline()) manager.setPlayerPermission(p.getPlayer(), perm, true);

                            sender.sendMessage("Permission " + ChatColor.GRAY + perm + ChatColor.RESET + " added to player " + ChatColor.GRAY + target + ChatColor.RESET + ".");

                            break;

                        case "remove":

                            database.removePermission(p.getUniqueId().toString(), perm);

                            if (p.isOnline()) manager.removePlayerPermission(p.getPlayer(), perm);

                            sender.sendMessage("Permission " + ChatColor.GRAY + perm + ChatColor.RESET + " removed from player " + ChatColor.GRAY + target + ChatColor.RESET + ".");

                            break;

                        case "negate":

                            database.setPermission(p.getUniqueId().toString(), perm, false);

                            if (p.isOnline()) manager.setPlayerPermission(p.getPlayer(), perm, false);

                            sender.sendMessage("Permission " + ChatColor.GRAY + perm + ChatColor.RESET + " negated on player " + ChatColor.GRAY + target + ChatColor.RESET + ".");

                            break;

                        default:

                            sender.sendMessage(ChatColor.RED + "Invalid operation. Must be " + ChatColor.RESET + "add" + ChatColor.RED + ", " + ChatColor.RESET + "remove" + ChatColor.RED + ", or " + ChatColor.RESET + "negate" + ChatColor.RED + ".");

                            break;

                    }

                } catch (DatabaseException e) {

                    sender.sendMessage(ChatColor.RED + "A database exception has occurred.");

                    e.printStackTrace();

                }

            }

        } else {

            sender.sendMessage(ChatColor.RED + "Access denied.");

        }

    }

    private void setUserGroup(CommandSender sender, String target, String group, String operation) {

        if (sender.hasPermission("kxperms.command.setusergroup")) {

            if (group == null) {

                sender.sendMessage(ChatColor.RED + "You must specify a group to add or remove.");

            } else {

                @SuppressWarnings("deprecation") OfflinePlayer p = Bukkit.getOfflinePlayer(target);

                try {

                    switch (operation.toLowerCase()) {

                        case "add":

                            if (!database.listGroups().contains(group)) {

                                sender.sendMessage(ChatColor.RED + "Group " + ChatColor.RESET + group + ChatColor.RED + " does not exist.");

                            } else {

                                database.addGroup(p.getUniqueId().toString(), group);

                                if (p.isOnline()) manager.addPlayerGroup(p.getPlayer(), group);

                                sender.sendMessage("Group " + ChatColor.GRAY + group + ChatColor.RESET + " added to player " + ChatColor.GRAY + target + ChatColor.RESET + ".");

                            }

                            break;

                        case "remove":

                            if (p.isOnline()) manager.removePlayerGroup(p.getPlayer(), group);

                            database.removeGroup(p.getUniqueId().toString(), group);

                            sender.sendMessage("Group " + ChatColor.GRAY + group + ChatColor.RESET + " removed from player " + ChatColor.GRAY + target + ChatColor.RESET + ".");

                            break;

                        default:

                            sender.sendMessage(ChatColor.RED + "Invalid operation. Must be " + ChatColor.RESET + "add" + ChatColor.RED + " or " + ChatColor.RESET + "remove" + ChatColor.RED + ".");

                            break;

                    }

                } catch (DatabaseException e) {

                    sender.sendMessage(ChatColor.RED + "A database exception has occurred.");

                    e.printStackTrace();

                }

            }

        } else {

            sender.sendMessage(ChatColor.RED + "Access denied.");

        }

    }

    private void setUserPrefix(CommandSender sender, String target, @Nullable String prefix, String operation) {

        if (sender.hasPermission("kxperms.command.setuserprefix")) {

            if (prefix == null && !operation.equalsIgnoreCase("remove") && !operation.equalsIgnoreCase("view")) {

                sender.sendMessage(ChatColor.RED + "You must specify a prefix to set.");

            } else {

                @SuppressWarnings("deprecation") OfflinePlayer p = Bukkit.getOfflinePlayer(target);

                try {

                    switch (operation.toLowerCase()) {

                        case "add":

                            database.setPrefix(p.getUniqueId().toString(), prefix);

                            if (p.isOnline()) manager.setPlayerPrefix(p.getPlayer(), prefix);

                            sender.sendMessage("Prefix of player " + ChatColor.GRAY + target + ChatColor.RESET + " set to " + ChatColor.GRAY + prefix);

                            break;

                        case "remove":

                            database.removePrefix(p.getUniqueId().toString());

                            if (p.isOnline()) manager.removePlayerPrefix(p.getPlayer());

                            sender.sendMessage("Prefix of player " + ChatColor.GRAY + target + ChatColor.RESET + " removed.");

                            break;

                        case "view":

                            String currentPrefix = database.getPrefix(p.getUniqueId().toString());

                            if (currentPrefix == null) {

                                sender.sendMessage("Player " + ChatColor.GRAY + target + ChatColor.RESET + " has no prefix.");

                            } else {

                                sender.sendMessage("Prefix of player " + ChatColor.GRAY + target + ChatColor.RESET + " is: " + ChatColor.translateAlternateColorCodes('&', currentPrefix));

                            }

                            break;

                        default:

                            sender.sendMessage(ChatColor.RED + "Invalid operation. Must be " + ChatColor.RESET + "add" + ChatColor.RED + ", " + ChatColor.RESET + "remove" + ChatColor.RED + ", or " + ChatColor.RESET + "view" + ChatColor.RESET + ".");

                            break;

                    }

                } catch (DatabaseException e) {

                    sender.sendMessage(ChatColor.RED + "A database exception has occurred.");

                    e.printStackTrace();

                }

            }

        } else {

            sender.sendMessage(ChatColor.RED + "Access denied.");

        }

    }

    private void setUserSuffix(CommandSender sender, String target, @Nullable String suffix, String operation) {

        if (sender.hasPermission("kxperms.command.setusersuffix")) {

            if (suffix == null && !operation.equalsIgnoreCase("remove") && !operation.equalsIgnoreCase("view")) {

                sender.sendMessage(ChatColor.RED + "You must specify a suffix to set.");

            } else {

                @SuppressWarnings("deprecation") OfflinePlayer p = Bukkit.getOfflinePlayer(target);

                try {

                    switch (operation.toLowerCase()) {

                        case "add":

                            database.setSuffix(p.getUniqueId().toString(), suffix);

                            if (p.isOnline()) manager.setPlayerSuffix(p.getPlayer(), suffix);

                            sender.sendMessage("Suffix of player " + ChatColor.GRAY + target + ChatColor.RESET + " set to " + ChatColor.GRAY + suffix);

                            break;

                        case "remove":

                            database.removeSuffix(p.getUniqueId().toString());

                            if (p.isOnline()) manager.removePlayerSuffix(p.getPlayer());

                            sender.sendMessage("Suffix of player " + ChatColor.GRAY + target + ChatColor.RESET + " removed.");

                            break;

                        case "view":

                            String currentSuffix = database.getSuffix(p.getUniqueId().toString());

                            if (currentSuffix == null) {

                                sender.sendMessage("Player " + ChatColor.GRAY + target + ChatColor.RESET + " has no suffix.");

                            } else {

                                sender.sendMessage("Suffix of player " + ChatColor.GRAY + target + ChatColor.RESET + " is: " + ChatColor.translateAlternateColorCodes('&', currentSuffix));

                            }

                            break;

                        default:

                            sender.sendMessage(ChatColor.RED + "Invalid operation. Must be " + ChatColor.RESET + "add" + ChatColor.RED + ", " + ChatColor.RESET + "remove" + ChatColor.RED + ", or " + ChatColor.RESET + "view" + ChatColor.RESET + ".");

                            break;

                    }

                } catch (DatabaseException e) {

                    sender.sendMessage(ChatColor.RED + "A database exception has occurred.");

                    e.printStackTrace();

                }

            }

        } else {

            sender.sendMessage(ChatColor.RED + "Access denied.");

        }

    }

    private void deleteUser(CommandSender sender, String target) {

        if (sender.hasPermission("kxperms.command.deleteuser")) {

            @SuppressWarnings("deprecation") OfflinePlayer p = Bukkit.getOfflinePlayer(target);

            try {

                database.deletePlayer(p.getUniqueId().toString());

                if (p.isOnline()) manager.deletePlayer(p.getPlayer());

                sender.sendMessage("Player " + ChatColor.GRAY + target + ChatColor.RESET + " deleted.");

            } catch (DatabaseException e) {

                sender.sendMessage(ChatColor.RED + "A database exception has occurred.");

                e.printStackTrace();

            }

        } else {

            sender.sendMessage(ChatColor.RED + "Access denied.");

        }

    }

    private void setGroupPerm(CommandSender sender, String target, String perm, String operation) {

        if (sender.hasPermission("kxperms.command.setgroupperm")) {

            if (perm == null) {

                sender.sendMessage(ChatColor.RED + "You must specify a permission to set.");

            } else {

                try {

                    if (database.listGroups().contains(target)) {

                        switch (operation.toLowerCase()) {

                            case "add":

                                database.setPermission(target, perm, true);

                                manager.setGroupPermission(target, perm, true);

                                sender.sendMessage("Permission " + ChatColor.GRAY + perm + ChatColor.RESET + " added to group " + ChatColor.GRAY + target + ChatColor.RESET + ".");

                                break;

                            case "remove":

                                database.removePermission(target, perm);

                                manager.removeGroupPermission(target, perm);

                                sender.sendMessage("Permission " + ChatColor.GRAY + perm + ChatColor.RESET + " removed from group " + ChatColor.GRAY + target + ChatColor.RESET + ".");

                                break;

                            case "negate":

                                database.setPermission(target, perm, false);

                                manager.setGroupPermission(target, perm, false);

                                sender.sendMessage("Permission " + ChatColor.GRAY + perm + ChatColor.RESET + " negated on group " + ChatColor.GRAY + target + ChatColor.RESET + ".");

                                break;

                            default:

                                sender.sendMessage(ChatColor.RED + "Invalid operation. Must be " + ChatColor.RESET + "add" + ChatColor.RED + ", " + ChatColor.RESET + "remove" + ChatColor.RED + ", or " + ChatColor.RESET + "negate" + ChatColor.RED + ".");

                                break;

                        }

                    } else {

                        sender.sendMessage(ChatColor.RED + "Group " + ChatColor.RESET + target + ChatColor.RED + " does not exist.");

                    }

                } catch (DatabaseException e) {

                    sender.sendMessage(ChatColor.RED + "A database exception has occurred.");

                    e.printStackTrace();

                }

            }

        } else {

            sender.sendMessage(ChatColor.RED + "Access denied.");

        }

    }

    private void setGroupParent(CommandSender sender, String target, String group, String operation) {

        if (sender.hasPermission("kxperms.command.setgroupparent")) {

            if (group == null) {

                sender.sendMessage(ChatColor.RED + "You must specify a parent to add or remove.");

            } else {

                try {

                    switch (operation.toLowerCase()) {

                        case "add":

                            if (target.equalsIgnoreCase(group)) {

                                sender.sendMessage(ChatColor.RED + "A group cannot be its own parent.");

                            } else if (target.equalsIgnoreCase("default")) {

                                sender.sendMessage(ChatColor.RED + "The default group cannot have parents.");

                            } else {

                                if (!database.listGroups().contains(group)) {

                                    sender.sendMessage(ChatColor.RED + "Group " + ChatColor.RESET + group + ChatColor.RED + " does not exist.");

                                } else {

                                    database.addGroup(target, group);

                                    manager.addGroupParent(target, group);

                                    sender.sendMessage("Parent " + ChatColor.GRAY + group + ChatColor.RESET + " added to group " + ChatColor.GRAY + target + ChatColor.RESET + ".");

                                }

                            }

                            break;

                        case "remove":

                            database.removeGroup(target, group);

                            manager.removeGroupParent(target, group);

                            sender.sendMessage("Parent " + ChatColor.GRAY + group + ChatColor.RESET + " removed from group " + ChatColor.GRAY + target + ChatColor.RESET + ".");

                            break;

                        default:

                            sender.sendMessage(ChatColor.RED + "Invalid operation. Must be " + ChatColor.RESET + "add" + ChatColor.RED + " or " + ChatColor.RESET + "remove" + ChatColor.RED + ".");

                            break;

                    }

                } catch (DatabaseException e) {

                    sender.sendMessage(ChatColor.RED + "A database exception has occurred.");

                    e.printStackTrace();

                }

            }

        } else {

            sender.sendMessage(ChatColor.RED + "Access denied.");

        }

    }

    private void setGroupPrefix(CommandSender sender, String target, String prefix, String operation) {

        if (sender.hasPermission("kxperms.command.setgroupprefix")) {

            if (prefix == null && !operation.equalsIgnoreCase("remove") && !operation.equalsIgnoreCase("view")) {

                sender.sendMessage(ChatColor.RED + "You must specify a prefix to set.");

            } else {

                try {

                    switch (operation.toLowerCase()) {

                        case "add":

                            manager.setGroupPrefix(target, prefix);

                            database.setPrefix(target, prefix);

                            sender.sendMessage("Prefix of group " + ChatColor.GRAY + target + ChatColor.RESET + " set to " + ChatColor.GRAY + prefix);

                            break;

                        case "remove":

                            manager.removeGroupPrefix(target);

                            database.removePrefix(target);

                            sender.sendMessage("Prefix of group " + ChatColor.GRAY + target + ChatColor.RESET + " removed.");

                            break;

                        case "view":

                            String currentPrefix = database.getPrefix(target);

                            if (currentPrefix == null) {

                                sender.sendMessage("Group " + ChatColor.GRAY + target + ChatColor.RESET + " has no prefix.");

                            } else {

                                sender.sendMessage("Prefix of group " + ChatColor.GRAY + target + ChatColor.RESET + " is: " + ChatColor.translateAlternateColorCodes('&', currentPrefix));

                            }

                            break;

                        default:

                            sender.sendMessage(ChatColor.RED + "Invalid operation. Must be " + ChatColor.RESET + "add" + ChatColor.RED + ", " + ChatColor.RESET + "remove" + ChatColor.RED + ", or " + ChatColor.RESET + "view" + ChatColor.RESET + ".");

                            break;

                    }

                } catch (DatabaseException e) {

                    sender.sendMessage(ChatColor.RED + "A database exception has occurred.");

                    e.printStackTrace();

                }

            }

        } else {

            sender.sendMessage(ChatColor.RED + "Access denied.");

        }

    }

    private void setGroupSuffix(CommandSender sender, String target, String suffix, String operation) {

        if (sender.hasPermission("kxperms.command.setgroupsuffix")) {

            if (suffix == null && !operation.equalsIgnoreCase("remove") && !operation.equalsIgnoreCase("view")) {

                sender.sendMessage(ChatColor.RED + "You must specify a suffix to set.");

            } else {

                try {

                    switch (operation.toLowerCase()) {

                        case "add":

                            manager.setGroupSuffix(target, suffix);

                            database.setSuffix(target, suffix);

                            sender.sendMessage("Suffix of group " + ChatColor.GRAY + target + ChatColor.RESET + " set to " + ChatColor.GRAY + suffix);

                            break;

                        case "remove":

                            manager.removeGroupSuffix(target);

                            database.removeSuffix(target);

                            sender.sendMessage("Suffix of group " + ChatColor.GRAY + target + ChatColor.RESET + " removed.");

                            break;

                        case "view":

                            String currentSuffix = database.getSuffix(target);

                            if (currentSuffix == null) {

                                sender.sendMessage("Group " + ChatColor.GRAY + target + ChatColor.RESET + " has no suffix.");

                            } else {

                                sender.sendMessage("Suffix of group " + ChatColor.GRAY + target + ChatColor.RESET + " is: " + ChatColor.translateAlternateColorCodes('&', currentSuffix));

                            }

                            break;

                        default:

                            sender.sendMessage(ChatColor.RED + "Invalid operation. Must be " + ChatColor.RESET + "add" + ChatColor.RED + ", " + ChatColor.RESET + "remove" + ChatColor.RED + ", or " + ChatColor.RESET + "view" + ChatColor.RESET + ".");

                            break;

                    }

                } catch (DatabaseException e) {

                    sender.sendMessage(ChatColor.RED + "A database exception has occurred.");

                    e.printStackTrace();

                }

            }

        } else {

            sender.sendMessage(ChatColor.RED + "Access denied.");

        }

    }

    private void createGroup(CommandSender sender, String target) {

        if (sender.hasPermission("kxperms.command.creategroup")) {

            try {

                if (database.listGroups().contains(target)) {

                    sender.sendMessage(ChatColor.RED + "Group " + ChatColor.RESET + target + ChatColor.RED + " already exists.");

                } else {

                    if (target.equalsIgnoreCase("list")) {

                        sender.sendMessage(ChatColor.RED + "The name " + ChatColor.RESET + "list" + ChatColor.RED + " is not permitted.");

                    } else if (target.equalsIgnoreCase("groups")) {

                        sender.sendMessage(ChatColor.RED + "The name " + ChatColor.RESET + "groups" + ChatColor.RED + " is not permitted.");

                    } else {

                        database.createGroup(target);

                        sender.sendMessage("Group " + ChatColor.GRAY + target + ChatColor.RESET + " created.");

                    }

                }

            } catch (DatabaseException e) {

                sender.sendMessage(ChatColor.RED + "A database exception has occurred.");

                e.printStackTrace();

            }

        } else {

            sender.sendMessage(ChatColor.RED + "Access denied.");

        }

    }

    private void deleteGroup(CommandSender sender, String target) {

        if (sender.hasPermission("kxperms.command.deletegroup")) {

            try {

                if (!database.listGroups().contains(target)) {

                    sender.sendMessage(ChatColor.RED + "Group " + ChatColor.RESET + target + ChatColor.RED + " does not exist.");

                } else {

                    database.deleteGroup(target);

                    manager.handleGroupDeletion(target);

                    sender.sendMessage("Group " + ChatColor.GRAY + target + ChatColor.RESET + " deleted.");

                }

            } catch (DatabaseException e) {

                sender.sendMessage(ChatColor.RED + "A database exception has occurred.");

                e.printStackTrace();

            }

        } else {

            sender.sendMessage(ChatColor.RED + "Access denied.");

        }

    }

    public void changeDatabase(PermissionsDatabase database) {

        this.database = database;

    }

}

package com.kaixeleron.perms.command;

import com.kaixeleron.perms.PermissionsMain;
import com.kaixeleron.perms.permission.PermissionManager;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashMap;
import java.util.Map;

public class SuCommand implements CommandExecutor {

    private final PermissionsMain main;

    private final PermissionManager manager;

    private final TextComponent confirm;

    private final Map<Player, OfflinePlayer> confirming;
    private final Map<Player, BukkitRunnable> runnables;

    public SuCommand(PermissionsMain main, PermissionManager manager) {

        this.main = main;

        this.manager = manager;

        confirm = new TextComponent("Click ");

        TextComponent yes = new TextComponent("here");
        yes.setColor(net.md_5.bungee.api.ChatColor.GREEN);
        yes.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/su c"));
        confirm.addExtra(yes);

        TextComponent toConfirm = new TextComponent(" to confirm, or click ");
        toConfirm.setColor(net.md_5.bungee.api.ChatColor.RESET);
        confirm.addExtra(toConfirm);

        TextComponent no = new TextComponent("here");
        no.setColor(net.md_5.bungee.api.ChatColor.RED);
        no.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/su d"));
        confirm.addExtra(no);

        TextComponent toCancel = new TextComponent(" to cancel.");
        toCancel.setColor(net.md_5.bungee.api.ChatColor.RESET);
        confirm.addExtra(toCancel);

        confirming = new HashMap<>();
        runnables = new HashMap<>();

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (sender instanceof Player) {

            if (args.length > 0) {

                switch (args[0].toLowerCase()) {

                    case "c": // confirm

                        if (confirming.containsKey(sender)) {

                            OfflinePlayer target = confirming.get(sender);

                            manager.addSubstitute((Player) sender, target);
                            confirming.remove(sender);

                            manager.simulateRelog((Player) sender);

                            sender.sendMessage("You are now using " + ChatColor.GRAY + target.getName() + ChatColor.RESET + "'s permissions. Type " + ChatColor.GRAY + "/su e" + ChatColor.RESET + " to exit.");

                        } else {

                            sender.sendMessage(ChatColor.RED + "You have no pending user to substitute.");

                        }

                        break;

                    case "d": // deny

                        if (confirming.containsKey(sender)) {

                            confirming.remove(sender);

                            sender.sendMessage("Permission substitute cancelled.");

                        } else {

                            sender.sendMessage(ChatColor.RED + "You have no pending user to substitute.");

                        }

                        break;

                    case "e": // exit

                        if (manager.isSubstituting((Player) sender)) {

                            manager.removeSubstitute((Player) sender);

                            manager.simulateRelog((Player) sender);

                            sender.sendMessage("Your permissions have been returned to normal.");

                        } else {

                            sender.sendMessage(ChatColor.RED + "Your permissions are not being substituted for that of another player.");

                        }

                        break;

                    default:

                        if (runnables.containsKey(sender)) {

                            runnables.get(sender).cancel();

                        }

                        @SuppressWarnings("deprecation") OfflinePlayer target = Bukkit.getOfflinePlayer(args[0]);

                        confirming.put((Player) sender, target);

                        BukkitRunnable runnable = new BukkitRunnable() {

                            @Override
                            public void run() {

                                runnables.remove(sender);
                                confirming.remove(sender);

                            }

                        };

                        runnables.put((Player) sender, runnable);

                        runnable.runTaskLater(main, 300L);

                        sender.sendMessage("Are you sure that you want to substitute your permissions for that of " + ChatColor.GRAY + args[0] + ChatColor.RESET + "? This will simulate a relog.");

                        if (sender.isOp() && !target.isOp()) {

                            sender.sendMessage(ChatColor.RED + "The target player is not an operator. Your operator status needs to be removed for an accurate permission representation.");

                        } else if (!sender.isOp() && target.isOp()) {

                            sender.sendMessage(ChatColor.RED + "The target player is an operator. You need operator status for an accurate permission representation.");

                        }

                        sender.spigot().sendMessage(confirm);

                        break;

                }

            } else {

                sender.sendMessage("Substitute your permissions for that of another player.");
                sender.sendMessage(ChatColor.RED + "Usage: " + ChatColor.RESET + "/su <Player>");

            }

        } else {

            sender.sendMessage("This command can only be used by a player.");

        }

        return true;

    }

}

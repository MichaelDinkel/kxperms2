package com.kaixeleron.perms;

import com.kaixeleron.perms.database.DatabaseException;
import com.kaixeleron.perms.database.PermissionsDatabase;
import com.kaixeleron.perms.permission.PermissionManager;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.UUID;

public class PermissionsAPI {

    private final PermissionsMain m;

    private PermissionsDatabase database;

    private final PermissionManager manager;

    PermissionsAPI(PermissionsMain m, PermissionsDatabase database, PermissionManager manager) {

        this.m = m;

        this.database = database;

        this.manager = manager;

        PermissionsAPI.instance = this;

    }

    private static PermissionsAPI instance;

    /**
     * Get a singleton instance of the API.
     * @return An instance of the API.
     */
    public static PermissionsAPI getAPI() {

        return instance;

    }

    /**
     * Set the database to be used by all kxPerms functions.
     * @param database A class implementing {@link com.kaixeleron.perms.database.PermissionsDatabase} to be used as the new database.
     */
    public void setDatabase(PermissionsDatabase database) {

        this.database = database;
        m.changeDatabase(database);

    }

    /**
     * Set a permission on a player based on UUID.
     * @param id The UUID of the player.
     * @param permission The permission to set.
     * @param value The value to set.
     * @throws DatabaseException If an exception occurs while accessing the database.
     */
    public void setPlayerPermission(UUID id, String permission, boolean value) throws DatabaseException {
        
        database.setPermission(id.toString(), permission, value);

        Player p = Bukkit.getPlayer(id);
        
        if (p != null) manager.setPlayerPermission(p, permission, value);

    }

    /**
     * Set a permission on a player.
     * @param player The player in question.
     * @param permission The permission to set.
     * @param value The value to set.
     * @throws DatabaseException If an exception occurs while accessing the database.
     */
    public void setPlayerPermission(Player player, String permission, boolean value) throws DatabaseException {

        database.setPermission(player.getUniqueId().toString(), permission, value);
        manager.setPlayerPermission(player, permission, value);

    }

    /**
     * Remove a permission from a player based on UUID.
     * @param id The UUID of the player.
     * @param permission The permission to remove.
     * @throws DatabaseException If an exception occurs while accessing the database.
     */
    public void removePlayerPermission(UUID id, String permission) throws DatabaseException {

        database.removePermission(id.toString(), permission);

        Player p = Bukkit.getPlayer(id);

        if (p != null) manager.removePlayerPermission(p, permission);

    }

    /**
     * Remove a permission from a player.
     * @param player The player in question.
     * @param permission The permission to remove.
     * @throws DatabaseException  If an exception occurs while accessing the database.
     */
    public void removePlayerPermission(Player player, String permission) throws DatabaseException {

        database.removePermission(player.getUniqueId().toString(), permission);
        manager.removePlayerPermission(player, permission);

    }

    /**
     * Add a group to a player based on UUID.
     * @param id The UUID of the player.
     * @param group The group to add.
     * @throws DatabaseException If an exception occurs while accessing the database.
     */
    public void addPlayerGroup(UUID id, String group) throws DatabaseException {

        database.addGroup(id.toString(), group);

        Player p = Bukkit.getPlayer(id);

        if (p != null) manager.addPlayerGroup(p, group);

    }

    /**
     * Add a group to a player.
     * @param player The player in question.
     * @param group The group to add.
     * @throws DatabaseException If an exception occurs while accessing the database.
     */
    public void addPlayerGroup(Player player, String group) throws DatabaseException {

        database.addGroup(player.getUniqueId().toString(), group);
        manager.addPlayerGroup(player, group);

    }

    /**
     * Remove a group from a player based on UUID.
     * @param id The UUID of the player.
     * @param group The group to remove.
     * @throws DatabaseException If an exception occurs while accessing the database.
     */
    public void removePlayerGroup(UUID id, String group) throws DatabaseException {

        database.removeGroup(id.toString(), group);

        Player p = Bukkit.getPlayer(id);

        if (p != null) manager.removePlayerGroup(p, group);

    }

    /**
     * Remove a group from a player.
     * @param player The player in question.
     * @param group The group to remove.
     * @throws DatabaseException If an exception occurs while accessing the database.
     */
    public void removePlayerGroup(Player player, String group) throws DatabaseException {

        database.removeGroup(player.getUniqueId().toString(), group);
        manager.removePlayerGroup(player, group);

    }

    /**
     * Set the prefix of a player based on UUID.
     * @param id The UUID of the player.
     * @param prefix The prefix to set.
     * @throws DatabaseException If an exception occurs while accessing the database.
     */
    public void setPlayerPrefix(UUID id, String prefix) throws DatabaseException {

        database.setPrefix(id.toString(), prefix);

        Player p = Bukkit.getPlayer(id);

        if (p != null) manager.setPlayerPrefix(p, prefix);

    }

    /**
     * Set the prefix of a player.
     * @param player The player in question.
     * @param prefix The prefix to set.
     * @throws DatabaseException If an exception occurs while accessing the database.
     */
    public void setPlayerPrefix(Player player, String prefix) throws DatabaseException {

        database.setPrefix(player.getUniqueId().toString(), prefix);
        manager.setPlayerPrefix(player, prefix);

    }

    /**
     * Set the suffix of a player based on UUID.
     * @param id The UUID of the player.
     * @param suffix The suffix to set.
     * @throws DatabaseException If an exception occurs while accessing the database.
     */
    public void setPlayerSuffix(UUID id, String suffix) throws DatabaseException {

        database.setSuffix(id.toString(), suffix);

        Player p = Bukkit.getPlayer(id);

        if (p != null) manager.setPlayerSuffix(p, suffix);

    }

    /**
     * Set the suffix of a player.
     * @param player The player in question.
     * @param suffix The prefix to set.
     * @throws DatabaseException If an exception occurs while accessing the database.
     */
    public void setPlayerSuffix(Player player, String suffix) throws DatabaseException {

        database.setSuffix(player.getUniqueId().toString(), suffix);
        manager.setPlayerSuffix(player, suffix);

    }

    /**
     * Remove the prefix of a player based on UUID.
     * @param id The UUID of the player.
     * @throws DatabaseException If an exception occurs while accessing the database.
     */
    public void removePlayerPrefix(UUID id) throws DatabaseException {

        database.removePrefix(id.toString());

        Player p = Bukkit.getPlayer(id);

        if (p != null) manager.removePlayerPrefix(p);

    }

    /**
     * Remove the prefix of a player.
     * @param player The player in question.
     * @throws DatabaseException If an exception occurs while accessing the database.
     */
    public void removePlayerPrefix(Player player) throws DatabaseException {

        database.removePrefix(player.getUniqueId().toString());
        manager.removePlayerPrefix(player);

    }

    /**
     * Remove the suffix of a player based on UUID.
     * @param id The UUID of the player.
     * @throws DatabaseException If an exception occurs while accessing the database.
     */
    public void removePlayerSuffix(UUID id) throws DatabaseException {

        database.removeSuffix(id.toString());

        Player p = Bukkit.getPlayer(id);

        if (p != null) manager.removePlayerSuffix(p);

    }

    /**
     * Remove the suffix of a player.
     * @param player The player in question.
     * @throws DatabaseException If an exception occurs while accessing the database.
     */
    public void removePlayerSuffix(Player player) throws DatabaseException {

        database.removeSuffix(player.getUniqueId().toString());
        manager.removePlayerSuffix(player);

    }

    /**
     * Get the prefix of a player based on UUID.
     * @param id The UUID of the player.
     * @return The prefix of the player or null if none is set.
     * @throws DatabaseException If an exception occurs while accessing the database.
     */
    public String getPlayerPrefix(UUID id) throws DatabaseException{

        return database.getPrefix(id.toString());

    }

    /**
     * Get the prefix of a player.
     * @param player The player in question.
     * @return The prefix of a player or null if none is set.
     */
    public String getPlayerPrefix(Player player) {

        return manager.getPlayerSuffix(player);

    }

    /**
     * Get the suffix of a player based on UUID.
     * @param id The UUID of the player.
     * @return The suffix of the player or null if none is set.
     * @throws DatabaseException If an exception occurs while accessing the database.
     */
    public String getPlayerSuffix(UUID id) throws DatabaseException {

        return database.getSuffix(id.toString());

    }

    /**
     * Get the suffix of a player.
     * @param player The player in question.
     * @return If an exception occurs while accessing the database.
     */
    public String getPlayerSuffix(Player player) {

        return manager.getPlayerSuffix(player);

    }

    /**
     * Set a permission on a group.
     * @param group The group in question.
     * @param permission The permission to set.
     * @param value The value to set.
     * @throws DatabaseException If an exception occurs while accessing the database.
     */
    public void setGroupPermission(String group, String permission, boolean value) throws DatabaseException {

        database.setPermission(group, permission, value);
        manager.setGroupPermission(group, permission, value);

    }

    /**
     * Remove a permission from a group.
     * @param group The group in question.
     * @param permission The permission to remove.
     * @throws DatabaseException If an exception occurs while accessing the database.
     */
    public void removeGroupPermission(String group, String permission) throws DatabaseException {

        database.removePermission(group, permission);
        manager.removeGroupPermission(group, permission);

    }

    /**
     * Add a parent to a group.
     * @param group The group in question.
     * @param parent The parent to add.
     * @throws DatabaseException If an exception occurs while accessing the database.
     */
    public void addGroupParent(String group, String parent) throws DatabaseException {

        database.addGroup(group, parent);
        manager.addGroupParent(group, parent);

    }

    /**
     * Remove a parent from a group.
     * @param group The group in question.
     * @param parent The parent ro remove
     * @throws DatabaseException If an exception occurs while accessing the database.
     */
    public void removeGroupParent(String group, String parent) throws DatabaseException {

        database.removeGroup(group, parent);
        manager.removeGroupParent(group, parent);

    }

    /**
     * Set the prefix of a group.
     * @param group The group in question.
     * @param prefix The prefix to set.
     * @throws DatabaseException If an exception occurs while accessing the database.
     */
    public void setGroupPrefix(String group, String prefix) throws DatabaseException {

        database.setPrefix(group, prefix);
        manager.setGroupPrefix(group, prefix);

    }

    /**
     * Set the suffix of a group.
     * @param group The group in question.
     * @param suffix The suffix to set.
     * @throws DatabaseException If an exception occurs while accessing the database.
     */
    public void setGroupSuffix(String group, String suffix) throws DatabaseException {

        database.setSuffix(group, suffix);
        manager.setGroupSuffix(group, suffix);

    }

    /**
     * Remove the prefix of a group.
     * @param group The group in question.
     * @throws DatabaseException If an exception occurs while accessing the database.
     */
    public void removeGroupPrefix(String group) throws DatabaseException {

        database.removePrefix(group);
        manager.removeGroupPrefix(group);

    }

    /**
     * Remove the suffix of a group.
     * @param group The group in question.
     * @throws DatabaseException If an exception occurs while accessing the database.
     */
    public void removeGroupSuffix(String group) throws DatabaseException {

        database.removeSuffix(group);
        manager.removeGroupSuffix(group);

    }

    /**
     * Delete a player from the database and clear all permissions based on UUID.
     * @param id The UUID of the player.
     * @throws DatabaseException If an exception occurs while accessing the database.
     */
    public void deletePlayer(UUID id) throws DatabaseException {

        database.deletePlayer(id.toString());

        Player p = Bukkit.getPlayer(id);

        if (p != null) manager.deletePlayer(p);

    }

    /**
     * Delete a player from the database and clear all permissions.
     * @param player The player in question.
     * @throws DatabaseException If an exception occurs while accessing the database.
     */
    public void deletePlayer(Player player) throws DatabaseException {

        database.deletePlayer(player.getUniqueId().toString());
        manager.deletePlayer(player);

    }

    /**
     * Create a group.
     * @param name Name of the group to create.
     * @throws DatabaseException If an exception occurs while accessing the database.
     */
    public void createGroup(String name) throws DatabaseException {

        database.createGroup(name);

    }

    /**
     * Delete a group.
     * @param name The group to delete.
     * @throws DatabaseException If an exception occurs while accessing the database.
     */
    public void deleteGroup(String name) throws DatabaseException {

        manager.handleGroupDeletion(name);
        database.deleteGroup(name);

    }

}
